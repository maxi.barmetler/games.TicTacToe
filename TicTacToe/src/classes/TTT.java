package classes;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class TTT extends Application
{
	Scene scene;

	BorderPane mainPane;

	// Playground:
	GridPane playground;
	GameButton[][] buttons;

	// Game vars:
	String activePlayer = "o";
	String lastWinner = "n";

	public void enter(int x, int y)
	{
		if (!buttons[x][y].state.equals("clear"))
		{

		} else
		{
			buttons[x][y].setState(activePlayer);
			String winner = checkforWin();
			if (!winner.equals("n"))
			{
				System.out.println("Winner: " + winner);
			} else if (activePlayer.equals("x"))
			{
				activePlayer = "o";
			} else
			{
				activePlayer = "x";
			}
		}
		updateButtons();
	}

	public String checkforWin()
	{
		int[][] states = new int[3][3];

		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				switch (buttons[x][y].state)
				{
					case "x" :
						states[x][y] = 1;
						break;

					case "o" :
						states[x][y] = 2;
						break;

					case "n" :
						states[x][y] = 0;
						break;
				}
			}
		}

		for (int i = 0; i < 3; i++)
		{
			if (states[0][i] + states[1][i] + states[2][i] > 0
					&& states[0][i] == states[1][i]
					&& states[1][i] == states[2][i])
				return activePlayer;

			if (states[i][0] + states[i][1] + states[i][2] > 0
					&& states[i][0] == states[i][1]
					&& states[i][1] == states[i][2])
				return activePlayer;
		}

		if (states[0][0] + states[1][1] + states[2][2] > 0
				&& states[0][0] == states[1][1] && states[1][1] == states[2][2])
			return activePlayer;

		if (states[0][2] + states[1][1] + states[2][0] > 0
				&& states[0][2] == states[1][1] && states[1][1] == states[2][0])
			return activePlayer;
		return "n";
	}

	public void init()
	{
		Images.loadImages();

		mainPane = new BorderPane();
		scene = new Scene(mainPane, 500, 500);

		initPlayground();
	}

	public void initPlayground()
	{
		playground = new GridPane();
		playground.setPadding(new Insets(2));

		buttons = new GameButton[3][3];
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				buttons[x][y] = new GameButton();
				playground.add(buttons[x][y], x, y);
				buttons[x][y].update(scene.getWidth(), scene.getHeight());

				int a = x;
				int b = y;
				buttons[x][y].setOnAction(e ->
				{
					this.enter(a, b);
				});
			}
		}

		mainPane.setCenter(playground);
	}

	public void start(Stage stage)
	{
		mainPane.setId("pane");
		scene.getStylesheets().addAll(this.getClass()
				.getResource("../styles/style.css").toExternalForm());

		scene.heightProperty().addListener(e ->
		{
			updateButtons();
		});
		scene.widthProperty().addListener(e ->
		{
			updateButtons();
		});

		stage.setTitle("TTT");
		stage.getIcons().add(new Image(
				getClass().getResourceAsStream("../images/icon.png")));
		stage.setScene(scene);
		stage.setMinHeight(256);
		stage.setMinWidth(256);
		stage.show();
	}

	public void updateButtons()
	{
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				buttons[x][y].update(scene.getWidth(), scene.getHeight());
			}
		}
	}

	public static void main(String[] args)
	{
		launch(args);
	}
}