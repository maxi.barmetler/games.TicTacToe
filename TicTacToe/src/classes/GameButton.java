package classes;

import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;

public class GameButton extends Button
{
	StackPane content;
	Rectangle grid;
	ImageView symbol;
	Color hoverc;
	boolean hovered, pressed;

	String state = "clear";

	public GameButton()
	{
		super();
		setMaxSize(1000, 1000);
		setPadding(new Insets(-2));
		content = new StackPane();
		setGraphic(content);
		setBackground(null);
		symbol = new ImageView();
		grid = new Rectangle(10, 10);
		grid.setFill(null);
		grid.setStroke(Color.BLACK);
		grid.setStrokeWidth(4);
		grid.setStrokeType(StrokeType.INSIDE);
		content.getChildren().addAll(grid, symbol);

		hoverc = Color.rgb(150, 170, 255, 0.2);

		setOnMouseEntered(e ->
		{
			hovered = true;
			updateColor();
		});

		setOnMouseExited(e ->
		{
			hovered = false;
			updateColor();
		});

		setOnMousePressed(e ->
		{
			pressed = true;
			updateColor();
		});

		setOnMouseReleased(e ->
		{
			pressed = false;
			updateColor();
		});
	}

	public void setState(String s)
	{
		state = s;
	}

	public void updateColor()
	{
		if (pressed && hovered)
		{
			grid.setFill(hoverc.darker().darker().darker().saturate());
			return;
		}
		if (pressed)
		{
			grid.setFill(hoverc.darker().darker().darker().desaturate()
					.desaturate().desaturate());
			return;
		}
		if (hovered)
		{
			grid.setFill(hoverc);
			return;
		}
		grid.setFill(null);
	}

	public void update(double w, double h)
	{
		grid.setWidth(w / 3 + 3);
		grid.setHeight(h / 3 + 3);
		symbol.setImage(Images.getImage("symbol_" + state));
		double size = 100;
		if (grid.getWidth() > grid.getHeight())
		{
			size = (grid.getHeight() - 16);
		} else
		{
			size = (grid.getWidth() - 16);
		}
		symbol.setFitWidth(size);
		symbol.setFitHeight(size);
	}
}