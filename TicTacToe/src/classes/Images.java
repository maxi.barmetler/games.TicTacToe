package classes;

import java.io.File;
import java.net.MalformedURLException;

import javafx.scene.image.Image;

public class Images
{
	static Image[] images = new Image[0];
	static String[] index = new String[0];
	static int size = 0;

	public static Image getImage(String s)
	{
		for (int i = 0; i < size; i++)
		{
			if (index[i].equals(s))
				return images[i];
		}
		System.out.println("fail");
		return null;
	}

	public static Image getImage(int i)
	{
		return images[i];
	}

	public static void loadImages()
	{
		File folder = new File("src/images");
		File[] files = folder.listFiles();
		size = files.length;
		images = new Image[size];
		index = new String[size];

		for (int i = 0; i < size; i++)
		{
			try
			{
				images[i] = new Image(files[i].toURL().toString());
				String name = files[i].getName();
				name = name.substring(0, name.length() - 4);
				index[i] = name;
			} catch (MalformedURLException e)
			{
				e.printStackTrace();
			}
		}
	}
}